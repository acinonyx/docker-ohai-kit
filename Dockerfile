# OHAI-Kit Docker image
#
# Copyright (C) 2021, 2022 Libre Space Foundation <https://libre.space/>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

ARG PYTHON_IMAGE_TAG=3.7.11
FROM python:${PYTHON_IMAGE_TAG}
LABEL org.opencontainers.image.authors='LSF operations team <ops@libre.space>'

ARG OHAI_REVISION=d8bc3b84
ARG OHAI_UID=999
ARG OHAI_HOME_DIR=/opt/ohai

ENV STATIC_ROOT=/opt/ohai/staticfiles
ENV MEDIA_ROOT=/opt/ohai/media

# Install 'pipenv' and 'gunicorn'
RUN pip install --no-cache-dir pipenv gunicorn

# Install 'npm' and 'sass'
RUN apt-get update \
	&& apt-get install -qy --no-install-recommends npm \
	&& rm -r /var/lib/apt/lists/*

# Create 'ohai' user and group
RUN groupadd -r -g ${OHAI_UID} ohai \
	&& useradd -r -g ohai -u ${OHAI_UID} -s /sbin/nologin -d ${OHAI_HOME_DIR} ohai \
	&& install -d ${OHAI_HOME_DIR}

# Change working directory
WORKDIR ${OHAI_HOME_DIR}

# Checkout OHAI-Kit
RUN git clone --no-checkout https://gitlab.com/lulzbot3d/ohai-kit.git . \
	&& git checkout ${OHAI_REVISION} \
	&& rm -rf .git \
	&& echo "\nSECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')" >> ohai/settings.py  # XXX: workaround for security middleware

# Create directories and volumes
RUN install -o ohai -g ohai -d \
	${STATIC_ROOT} \
	${MEDIA_ROOT} \
	${OHAI_HOME_DIR}/ohai/whoosh_index
VOLUME ["${STATIC_ROOT}", "${MEDIA_ROOT}", "${OHAI_HOME_DIR}/ohai/whoosh_index"]

# Install OHAI-Kit
RUN pipenv install --system \
	&& cd ohai_kit/static \
	&& npm install \
	&& rm -rf ~/.npm

# Add container entrypoint
COPY docker-entrypoint.sh /
ENTRYPOINT ["/docker-entrypoint.sh"]

# Expose server port
EXPOSE 8000

# Run application
CMD ["ohai"]
